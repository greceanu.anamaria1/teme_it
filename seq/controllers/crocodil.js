const CrocodilDb = require("../models").Crocodil;
const InsulaDb = require("../models").Insula;

const controller = {
    getAllCrocodiles: async(req, res) => {
        CrocodilDb.findAll().then((crocodili) => {
            res.status(200).send(crocodili);
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({message: "server error"});
        });
    },

    addCrocodile: async (req, res) => {
        const {idInsula, nume, prenume, varsta} = req.body;
        InsulaDb.findByPk(idInsula)
        .then((insula) => {
            console.log(insula);
            if (insula) {
                insula
                .createCrocodil({nume, prenume, varsta})
                .then((crocodil) => {
                    res.status(201).send(crocodil);
                }).catch((err) => {
                    console.log(err)
                    res.status(500).send({message: "server error"});
                });
            } else {
                res.status(404).send({message: "insula not found"});
            }
        }).catch((err) => {
            console.log(err);
            res.status(500).send({message: "server error"});
        });
    },

    deleteCrocodileById: async(req, res) => {
        const {id} = req.params;
        if(!id) {
            res.status(400).send({message: "ID not provided"});
        }

        CrocodilDb.destroy({
            where: {id: id}
        })
        .then(() => {
            res.status(200).send({message:"crocodil sters"});
        }). catch((error) => {
            console.log(error);
            res.status(500).send({message: "server error"})
        });
    },

   updateCrocodileById: async(req, res) => {
        const {id} = req.params;
        if(!id) {
            res.status(400).send({message: "ID not provided"});
        }
        const {nume, prenume, varsta} = req.body;
        CrocodilDb.update(req.body, {
            where: {id: id}
        })
        .then((crocodil) => {
            res.status(200).send({message:"crocodil modificat"});
        })
        .catch((error) => {
            console.log(error);
            res.status(500).send({message: "server error"})
        });
    }
};

module.exports = controller;