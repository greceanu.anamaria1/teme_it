const express = require("express");
const connection = require("./models").connection;
const router = require("./routes");
const app = express();

app.use(express.json());

let port = 8081;

app.use("/api", router);

app.get("/reset", (req, res) => {
    connection
    .sync({force: true})
    .then(() => {
        res.status(201).send({message: "database reset"});
    })
    .catch(() => {
        res.status(500).send({message: "database reset failed"});
    });
});

app.use("/*", (req, res) => {
    res.status(200).send("app ruleaza");
});

app.listen(port, () => {
    console.log("server is running " + port);
});